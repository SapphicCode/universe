{...}: {
  virtualisation.podman.enable = true;
  boot.initrd.systemd.enable = true;
}
